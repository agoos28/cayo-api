<?php 
//logs_helper.php
function log_add($message, $type='INFO')
{
	date_default_timezone_set('Asia/Jakarta');
	$log_file =  WRITEPATH ."/logs/log-". date("YmdH").".log";
	if(!is_file($log_file))
	{
		$str='Start file:'.date("Y-m-d H:i:s");
		error_log($str."\n",3, $log_file);
	}
	
	$str=date("Y-m-d H:i:s")."\t".trim(strtoupper($type))."\t".ltrim($message);
	error_log( $str."\n",3, $log_file);
	return ;
}