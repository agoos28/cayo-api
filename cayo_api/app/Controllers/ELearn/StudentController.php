<?php

namespace App\Controllers\ELearn;

//use App\Controllers\BaseController;
use App\Models\StudentModel;

// Import Excel Package
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use agungsugiarto\boilerplate\Controllers\BaseController;
use agungsugiarto\boilerplate\Entities\Collection;

use agungsugiarto\boilerplate\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use Myth\Auth\Authorization\PermissionModel;
use Myth\Auth\Entities\User;


class StudentController extends BaseController
{
    use ResponseTrait;
    /** @var \agungsugiarto\boilerplate\Models\UserModel */
    protected $users, $student;
	public $db;

    public function __construct()
    {
        $this->users = new UserModel();
        $this->student = new StudentModel();
		$this->db = \Config\Database::connect('elearning');
		helper("auth");
		helper("log"); //untuk development saja
		if (ENVIRONMENT == "development") 
		{
			$request = \Config\Services::request();
			$method =  @$request->getMethod(true);
			$inputs =  @$request->getVar();
			log_add("method:".$method."|".json_encode($inputs));
		}
		
    }

	public function index()
	{
		$uname= user();
		if($uname==NULL)
		{
			return redirect()->to(base_url('admin/user/profile'));
		}
		
		// $pass = 'HarleyQuin';
		// $str = password_hash($pass, PASSWORD_DEFAULT);

		$student_obj = new StudentModel();
		
		if ($this->request->isAJAX()) {
            $start = $this->request->getPost('start');
            $length = $this->request->getPost('length');
            $search = $this->request->getPost('search[value]');
			$order_pos = $this->request->getPost('order[0][column]'  );

            $order = @StudentModel::ORDERABLE[$order_pos];
            $dir = $this->request->getPost('order[0][dir]');

            return $this->respond(Collection::datatable(
                $student_obj->getResource($search)->orderBy($order, $dir)->limit($length, $start)->get()->getResultObject(),
                $student_obj->getResource()->countAllResults(),
                $student_obj->getResource($search)->countAllResults()
            ));
        }

		$students = [];
		return view("elearning/student_view", [
			"students" => $students,
		]);
	}
/**
     * Return the editable properties of a resource object.
     *
     * @param int id
     *
     * @return mixed
     */
	function updatePage($id=0)
	{
		$validationRules = [
            //'active'       => 'required',
            'mobile'     => "required|min_length[9]",
            'name'     => "required|alpha_numeric_space|min_length[3]",
            'email'        => "required|valid_email|is_unique[users.email,id,$id]",
            // 'password'     => 'if_exist',
            // 'pass_confirm' => 'matches[password]',
            // 'role'         => 'if_exist',
        ];
		$request = \Config\Services::request();
		$method =  $this->request->getMethod(true);
		$inputs =  $this->request->getPost();
		$student = $this->student->asArray()->find($id);
		if($this->request->getPost('csrf_test_name') == NULL)
		{
			return redirect()->back()->withInput();
		}
		
		if (!$this->validate($validationRules)) {
            return redirect()->back()->withInput()->with('error', $this->validator->getErrors());
        }
		
		
        $this->db->transBegin();

        try {
            $user =   new \stdClass();
            $user->branch = $this->request->getPost('branch');
            $user->mobile = $this->request->getPost('mobile');
            $user->email = $this->request->getPost('email');
            $user->name = $this->request->getPost('name');
			 
            $this->student->skipValidation(true)->update($id, $user);
 
            $this->db->transCommit();
        } catch (\Exception $e) {
            $this->db->transRollback();
            return redirect()->back()->with('sweet-error', $e->getMessage());
        }

        return redirect()->back()->with('sweet-success', "Berhasil update");
	}
	/**
     * Return the editable page of student.
     * ajax only/post
     * @param int id
     *
     * @return mixed
     */
    public function editPage($id=0)
    {
        $data = [
            'title'       => "Student",
            'subtitle'    => "Edit",
            'permissions' => $this->authorize->permissions(),
            'roles'       => $this->authorize->groups(),
            'student'        => $this->student->asArray()->find($id),
        ];
		
        return view('elearning/update_view', $data);
    }
	/**
     * Return the deleted properties of a resource object.
     * ajax only/post
     * @param int id
     *
     * @return mixed
     */
	function deletePage($id=0)
	{
		$this->db->transBegin();

        try {
            $user =   new \stdClass();
			//$userModel->where('id', 12)->delete(); 
            $found = $this->student->where('id', $id)->delete();  
            $this->db->transCommit();
        } catch (\Exception $e) {
            $this->db->transRollback();
            return $this->failNotFound('Tidak ditemukan');
        }

        return $this->respondDeleted($found, lang('Berhasil di hapus'));
	}
	/**
	Import / Upload
*/	
    public function uploadStudent()
    {
        if ($this->request->getMethod() == "post") {

            $file = $this->request->getFile("file");
			if (ENVIRONMENT == "development") 
			{
				log_add("membaca file sumber");
			}
            $inputFileName = $file->getTempName();
			
			$spreadsheet = IOFactory::load($inputFileName);
			$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			$total = count($sheetData);
			if (ENVIRONMENT == "development") 
			{
				log_add("konversi file sumber total:{$total}");
			}
			$session = session();

			$session->setFlashdata("success", "Data Asumsikan berhasil di upload :".$total );

			return redirect()->to(base_url('student/upload-student'));

        }
		
		if (ENVIRONMENT == "development") 
		{
			log_add("masalah pada link");
		}
		
		return redirect()->to(base_url('student'));
		
    }
/**
	Export / Download
*/
	public function downloadExcelReport()
	{
		$student_obj = new StudentModel();

		$students = $student_obj->findAll();

		$fileName = 'students.xlsx'; // File is to create

		$spreadsheet = new Spreadsheet();

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Id');
		$sheet->setCellValue('B1', 'Name');
		$sheet->setCellValue('C1', 'Email');
		$sheet->setCellValue('D1', 'Mobile');
		$sheet->setCellValue('E1', 'Branch');
		$rows = 2;

		foreach ($students as $val) {
			$sheet->setCellValue('A' . $rows, $val['id']);
			$sheet->setCellValue('B' . $rows, $val['name']);
			$sheet->setCellValue('C' . $rows, $val['email']);
			$sheet->setCellValue('D' . $rows, $val['mobile']);
			$sheet->setCellValue('E' . $rows, $val['branch']);
			$rows++;
		}

		if (ENVIRONMENT == "development") 
		{
			log_add("Pembuatan laporan row:{$rows}");
		}
		
		$writer = new Xlsx($spreadsheet);

		// file inside /public folder
		$dir = WRITEPATH . "export";
		if (!is_dir($dir)) {
			mkdir($dir);
		}
		$filepath = $dir.'/'.$fileName;

		$writer->save($filepath);

		if (ENVIRONMENT == "development") 
		{
			log_add("selesai pembuatan laporan");
		}

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');

		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filepath));
		flush(); // Flush system output buffer
		readfile($filepath);
		unlink( $filepath ); //hapus
		exit;
	}
}
/*
pastikan koneksi elearning tersedia
jalankan: php spark db:seed StudentSeeder
*/