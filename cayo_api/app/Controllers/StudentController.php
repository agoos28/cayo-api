<?php

namespace App\Controllers;

//use App\Controllers\BaseController;
use App\Models\StudentModel;

// Import Excel Package
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use agungsugiarto\boilerplate\Controllers\BaseController;
use agungsugiarto\boilerplate\Entities\Collection;
use agungsugiarto\boilerplate\Models\GroupModel;
use agungsugiarto\boilerplate\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use Myth\Auth\Authorization\PermissionModel;
use Myth\Auth\Entities\User;


class StudentController extends BaseController
{
    use ResponseTrait;

    /** @var \agungsugiarto\boilerplate\Models\UserModel */
    protected $users;
	public $db;

    public function __construct()
    {
        $this->users = new UserModel();
		$this->db = \Config\Database::connect('elearning');
    }
	
    public function uploadStudent()
    {
        if ($this->request->getMethod() == "post") {

            $file = $this->request->getFile("file");

            $inputFileName = $file->getTempName();
			
			$spreadsheet = IOFactory::load($inputFileName);
			$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			//echo '<pre>';var_dump($sheetData);


			//die($inputFileName);
/*
            $student = array();

            $csv_data = array_map('str_getcsv', file($file_name));

            if (count($csv_data) > 0) {

                $index = 0;

                foreach ($csv_data as $data) {

                    if ($index > 0) {

                        $student[] = array(
                            "name" => $data[1],
                            "email" => $data[2],
                            "mobile" => $data[3],
                            "designation" => $data[4],
                        );
                    }
                    $index++;
                }

                $builder = $this->db->table('tbl_students');

                $builder->insertBatch($student);
			}
*/
			$session = session();

			$session->setFlashdata("success", "Data Asumsikan berhasil di upload");

			return redirect()->to(base_url('student/upload-student'));

        }

		return redirect()->to(base_url('student'));
		
        return view("upload-file");
    }

	public function index()
	{
		$pass = 'HarleyQuin';
		$str = password_hash($pass, PASSWORD_DEFAULT);

		/*
		if ($this->request->isAJAX()) {
            $start = $this->request->getGet('start');
            $length = $this->request->getGet('length');
            $search = $this->request->getGet('search[value]');
            $order = UserModel::ORDERABLE[$this->request->getGet('order[0][column]')];
            $dir = $this->request->getGet('order[0][dir]');

            return $this->respond(Collection::datatable(
                $this->users->getResource($search)->orderBy($order, $dir)->limit($length, $start)->get()->getResultObject(),
                $this->users->getResource()->countAllResults(),
                $this->users->getResource($search)->countAllResults()
            ));
        }
		*/
		$student_obj = new StudentModel();

		$students = $student_obj->findAll();

		return view("elearning/student_view", [
			"students" => $students,
		]);
	}

	public function downloadExcelReport()
	{
		$student_obj = new StudentModel();

		$students = $student_obj->findAll();

		$fileName = 'students.xlsx'; // File is to create

		$spreadsheet = new Spreadsheet();

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Id');
		$sheet->setCellValue('B1', 'Name');
		$sheet->setCellValue('C1', 'Email');
		$sheet->setCellValue('D1', 'Mobile');
		$sheet->setCellValue('E1', 'Branch');
		$rows = 2;

		foreach ($students as $val) {
			$sheet->setCellValue('A' . $rows, $val['id']);
			$sheet->setCellValue('B' . $rows, $val['name']);
			$sheet->setCellValue('C' . $rows, $val['email']);
			$sheet->setCellValue('D' . $rows, $val['mobile']);
			$sheet->setCellValue('E' . $rows, $val['branch']);
			$rows++;
		}

		$writer = new Xlsx($spreadsheet);

		// file inside /public folder
		$dir = WRITEPATH . "export";
		if (!is_dir($dir)) {
			mkdir($dir);
		}
		$filepath = $dir.'/'.$fileName;

		$writer->save($filepath);

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');

		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filepath));
		flush(); // Flush system output buffer
		readfile($filepath);

		exit;
	}
}
/*
pastikan koneksi elearning tersedia
jalankan: php spark db:seed StudentSeeder
*/