<?php
namespace App\Controllers\Auth;
/**
 * Class UserCtrl
 * Route : development/LoginRoute
 */
use App\Models\UserModel;
use CodeIgniter\RESTful\ResourceController;
use Exception;
use \Firebase\JWT\JWT;
use Google_Client;
use Google_Service_Oauth2;

class UserCtrl extends ResourceController
{

    /**
     * Redirect to detail
     *
     * @return mixed
     */
	public function index()
	{
		return redirect()->to('loginExt/detail'); 	
		
	}

    /**
     * Return view detail. Only on test only (not production)
     *
     * @return mixed
     */
    public function details()
    {
		
		if( ENVIRONMENT != 'development' )
		{
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Nobody'  );
		}
		helper('log');
		
		$start = microtime(true);
		$config = config('App');
        $clientid = 	 $config->clientid;
        $clientsecret =  $config->clientsecret;
        $redirect_url =  $config->redirect_url;
		$email_debug = isset($config->email_debug)?$config->email_debug:[];
		//------------------
		$client = new Google_Client();
		$client->setClientId( $clientid );
		$client->setClientSecret( $clientsecret );
		$client->setRedirectUri( $redirect_url );
		//-----------------
		$client->addScope('profile');
		$client->addScope('email');
		//code
		$code = $this->request->getVar('code');
		if($code)
		{
			$session = session();
			$token = $client->fetchAccessTokenWithAuthCode( $code );
			if( ENVIRONMENT == 'development' )
			{
				log_add('code:'.$code.'|token:'.print_r($token,1) );

			}
			
			if(!isset($token['access_token']))
			{
				$response = [
					'status' => 401,
					'error' => true,
					'messages' => 'Access Token Invalid',
					'data' => [  ]
				];
				return $this->respondCreated($response);
			}

			//add to jwt
			$iat = time(); // current timestamp value
			$nbf = $iat + 10;
			$exp = $iat + 3600;

			$payload = [
				"tokens_details"=>[						
					"iat" => $iat, // issued at
					"nbf" => $nbf, //not before in seconds
					"exp" => $exp
				], // expire time in seconds
				"token_google" => $token,
			];
			
			$configUser = config('User');
			$key = $configUser->myKey;
			$tokenJWT = JWT::encode($payload, $key);
			$response['data']['token'] = $tokenJWT;
			$response['data']['tokenGoogle'] = $token['access_token'];
			
			$session->set('tokenJWT', $tokenJWT);
			$session->set('tokenGoogle', $token['access_token']);
			return redirect()->to('loginExt/detail?token='.substr($tokenJWT,0,7) );
			
		}
		
		//token
		$token = $this->request->getVar('token');
		if($token)
		{
			$session = session();
			$response = [
				'status' => 200,
				'error' => false,
				'messages' => 'Success',
				'data' => [
					'run' => microtime(true) - $start 
				]
			];

			$tokenJWT = $session->get('tokenJWT');
			$tokenGoogle = $session->get('tokenGoogle');
			$response['data']['token'] = $tokenJWT;
			$response['data']['tokenGoogle'] = $tokenGoogle;
			return $this->respondCreated($response);
		}
		
		if($code && isset($token['access_token']) )
		{
			$client->setAccessToken( $token );
			//----get details
			$gAuth = new Google_Service_Oauth2( $client );
			$google_info = $gAuth->userinfo->get();
			if( ENVIRONMENT == 'development' )
			{
				log_add('info:'.print_r($google_info,1) );
			}
			
			$email = $google_info->email;
			$name = $google_info->name;
			echo "Welcome {$name} ({$email})";

			if( ENVIRONMENT == 'development' && in_array($email, $email_debug ) )
			{
				log_add( print_r($google_info,1)."\ntoken:".print_r($token,1) ) ;

			}
			
		}
		else
		{
			echo "<a href='". $client->createAuthUrl(). "'>Login with Google</a>";
		}
		
	}

    /**
     * Return an array of resource objects, related to google Auth.
     *
     * @return mixed
     */
	public function info()
	{
		if( ENVIRONMENT != 'development' )
		{
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Nobody'  );
		}
		$start = microtime(true);
		$token = $this->request->getVar('token');//jwt
		$response = [
			'status' => 401,
			'error' => true,
			'messages' => 'Access denied',
			'data' => []
		];

		$config = config('User');
		$key = $config->myKey;

		if($token)
		{
			$config = config('User');			
			try {			
				$decoded = JWT::decode($token, $key, array("HS256"));
				if(isset($decoded->token_google))
				{
					$response = [
						'status' => 200,
						'error' => false,
						'messages' => 'Success',
						'data' => [
							'run' => microtime(true) - $start,
							'detail'=>$this->detailUser( $decoded->token_google  )
						]
					];
				}
				else
				{
					$response['messages'] = 'Unknown token';
					
				}
			} catch (Exception $ex) {
          
				$response = [
					'status' => 401,
					'error' => true,
					'messages' => 'Access denied',
					'data' => []
				];
				return $this->respondCreated($response);
			}
			
			return $this->respondCreated($response);
		}
		elseif(ENVIRONMENT == 'development')
		{
			$configUser = config('User');
			$token=[
				"access_token" => $configUser->access_token, 
				"expires_in" => $configUser->expires_in,
				"token_type" => $configUser->token_type ,
				"d_token" =>  $configUser->d_token,
				"created" =>  $configUser->created
			];

			//add to jwt
			$iat = time(); // current timestamp value
			$nbf = $iat + 10;
			$exp = $iat + 3600;

			$payload = [
				"tokens_details"=>[						
					"iat" => $iat, // issued at
					"nbf" => $nbf, //not before in seconds
					"exp" => $exp
				], // expire time in seconds
				"token_google" => $token,
			];
			
			$tokenJWT = JWT::encode($payload, $key);
			return $this->respondCreated($response);
		}
		
		return $this->respondCreated($response);
	}

    /**
     * Return an array of resource objects, related to google Auth.
     *
     * @return mixed
     */
	private function detailUser($token)
	{
		$config = config('App');
		$clientid = 	 $config->clientid;
		$clientsecret =  $config->clientsecret;
		$redirect_url =  $config->redirect_url;
		//----------------------------
		$client = new Google_Client();
		$client->setClientId( $clientid );
		$client->setClientSecret( $clientsecret );
		$client->setRedirectUri( $redirect_url );
		if(!is_array($token))
		{
			$token = (array) $token;
		}

		$client->setAccessToken( $token );
		$gAuth = new Google_Service_Oauth2( $client );
		$google_info = $gAuth->userinfo->get();
		return $google_info;
	}

}