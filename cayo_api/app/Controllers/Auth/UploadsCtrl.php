<?php
namespace App\Controllers\Auth;
use App\Controllers\BaseController;
use Exception;
/**
 * Class UploadsCtrl
 * Route : development/googleAuth, production/googleAuth
 */

class UploadsCtrl extends BaseController
{
    /**
     * Return an Image/file on uploads
     *
     * @return mixed
     */
    public function fileUsers()
    {
        $body = "nothing here";
        $request = \Config\Services::request();
        $nameFile = $request->getVar("f");

        $fileUser = WRITEPATH . "uploads/" . $nameFile;
        if (!is_file($fileUser)) {
            $fileUser = WRITEPATH . "uploads/pp_users/" . $nameFile;
        }

        if (!is_file($fileUser)) {
            if (ENVIRONMENT == "development") {
                log_add("404 load:" . print_r($nameFile, 1));
            }
            throw new \CodeIgniter\Exceptions\PageNotFoundException(
                "Image Not Found "
            );
        }

        $mimeType = "image/jpg";
        $this->response
            ->setStatusCode(200)
            ->setContentType($mimeType)
            ->setBody(file_get_contents($fileUser))
            ->send();
    }

}