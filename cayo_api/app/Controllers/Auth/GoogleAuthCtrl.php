<?php
namespace App\Controllers\Auth;

use App\Models\UserCayoModel;
use App\Models\Auths\AuthDeviceModel;

use CodeIgniter\RESTful\ResourceController;
use Exception;
use Firebase\JWT\JWT;
use Google_Client;
use Google_Service_Oauth2;
/**
 * Class GoogleAuth
 * Route : development/googleAuth, production/googleAuth
 */

class GoogleAuthCtrl extends ResourceController
{
    protected $users, $tblUsers;
    /**
     * Return start Controller.
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->db = \Config\Database::connect();
		$this->tblUsers = new UserCayoModel();
    }

    /**
     * Return an array of resource objects, related to google Auth.
     *
     * @return mixed
     */
    public function signin_post()
    {
        helper("log");
        $start = microtime(true);
        $request = \Config\Services::request();
		$tokenGoogle=NULL;
        $response_default = [
            "status" => 401,
            "error" => true,
            "messages" => "Access denied",
            "data" => [
                "token" => $tokenGoogle,
                "method" => $this->request->getMethod(true),
            ],
        ];
		
		if (ENVIRONMENT != "development") 
		{
			unset($response_default['data']);
		}
		
        $response_success = [
            "status" => 200,
            "error" => false,
            "messages" => "Success",
            "data" => [
                "run" => microtime(true) - $start,
            ],
        ];

        if ($this->request->getMethod(true) != "POST")
		{
            $response_default["messages"] = "Method Not Valid";
            return $this->respondCreated($response_default);
        }

		//check parameter (sebelum)
		$params = [
			'token'=>FILTER_SANITIZE_STRING,
			'platformId'=>FILTER_SANITIZE_NUMBER_INT,
			'deviceSsid'=>FILTER_SANITIZE_STRING,
			'deviceModel'=>FILTER_SANITIZE_STRING,
			'deviceSystemName'=>FILTER_SANITIZE_STRING,
			'deviceSystemVersion'=>FILTER_SANITIZE_STRING,
			'deviceName'=>FILTER_SANITIZE_STRING
		];
		//check parameter (sesudah)
		$params_require = [
			'token'=>FILTER_SANITIZE_STRING,
			'device_ssid'=>FILTER_SANITIZE_STRING, 
		];
		
		$input=[];
		foreach($params_require as $param => $filter)
		{
			$value = $request->getVar($param, $filter);
			if(!$value)
			{				
				$response_default["messages"] = "Parameters {$param} not Exists";
				$response_default['status'] = 400;
				$response_default['data'] = [];
				if (ENVIRONMENT == "development")
				{
					$inputs = $request->getPost();
					log_add("all input:" . print_r($inputs, 1));
					$response_default['data']['input'] = $inputs;
				}
				else
				{
					unset($response_default['data']);
				}
				return $this->respondCreated($response_default);
				
			}
			
			$input[$param] = $value;
			$$param = $value;
			
		}

		$require_sanitize=[
			'platform_id'=>FILTER_SANITIZE_NUMBER_INT,
			'device_model'=>FILTER_SANITIZE_STRING,
			'device_system_name'=>FILTER_SANITIZE_STRING,
			'device_system_version'=>FILTER_SANITIZE_STRING,
			'device_name'=>FILTER_SANITIZE_STRING
		];
		foreach($require_sanitize as $param => $filter)
		{
			$value = $request->getVar($param, $filter);
			//$input[$param] = $value;
			$$param = @$value;
			
		}

		unset($input);
        $tokenGoogle = $request->getVar("token");

        $start = microtime(true);
        $config = config("App");
        $clientid = $config->clientid;
        $clientsecret = $config->clientsecret;
        $redirect_url = $config->redirect_url;
        //------------------
        $client = new Google_Client();
        $client->setClientId($clientid);
        $client->setClientSecret($clientsecret);
        $client->setRedirectUri($redirect_url);
        //-----------------
        $client->addScope("profile");
        $client->addScope("email");

        try 
		{
            $client->setAccessToken($tokenGoogle);

            $gAuth = new Google_Service_Oauth2($client);
            $google_info = $gAuth->userinfo->get();
            if (ENVIRONMENT == "development") {
                log_add("info:" . print_r($google_info, 1));
            }

            if (!isset($google_info->email)) {
                $response_default["messages"] = "Google Token is not Valid";
                return $this->respondCreated($response_default);
            }
        } catch (Exception $ex) {
            $aInfo = json_decode($ex->getMessage());
            $code = 401;
            $message = "Access denied on token";
            if (is_object($aInfo) && isset($aInfo->error)) {
                $aInfo2 = $aInfo->error;
                if (isset($aInfo2->code)) {
                    $code = $aInfo2->code;
                    unset($aInfo2->code);
                }
                if (isset($aInfo2->message)) {
                    $message = $aInfo2->message;
                    unset($aInfo2->message);
                }

                $info = $aInfo2;
            } elseif (is_object($aInfo)) {
                $aInfo->cond = 1;
                if (isset($aInfo->code)) {
                    $code = $aInfo->code;
                    unset($aInfo->code);
                }
                if (isset($aInfo->message)) {
                    $message = $aInfo->message;
                    unset($aInfo->message);
                }

                $info = $aInfo;
            } else {
                $info = $ex->getMessage();
            }

            $response = [
                "status" => $code,
                "error" => true,
                "messages" => $message,
                // "data" => [
					// 'error'=>@$info->errors->, 
				// ],
				//$tokenGoogle, $clientid, $clientsecret
            ];
            return $this->respondCreated($response);
        }

        $email = $google_info->email;
        $name = $google_info->name;
        $picturepic = $google_info->picture;
        $info = [
            "name" => $name,
            "email" => $email,
            "password" => substr($tokenGoogle, 0, 10),
            "picturepic" => $picturepic,
        ];

        $this->register_user($info);
        $userInfo = $this->detail_user($email, "email");
		//input device 
		$infoDevice=[
			'user_id'=>$userInfo['id'],
			'platform_id'=> (int)$platform_id,
			'device_ssid'=>$device_ssid,
			'device_model'=>$device_model,
			'device_system_name'=>$device_system_name,//???
			'device_system_version'=>$device_system_version,
			'device_name'=>$device_name
		];
		//print_r($infoDevice);die;
		$this->register_device($infoDevice);
		$userInfo = $this->detail_user($email, "email");

        //--------------SAVE TO JWT--------------
        $iat = time(); // current timestamp value
        $nbf = $iat + 10;
        $exp = $iat + 3600;

        $payload = [
            "tokens_details" => [
                "iat" => $iat, // issued at
                "nbf" => $nbf, //not before in seconds
                "exp" => $exp,
            ], // expire time in seconds
            "token_google" => $tokenGoogle,
            "user_info" => $userInfo,
            "gAuth" => $google_info,
        ];

        $configUser = config("User");
        $key = $configUser->myKey;
        $tokenJWT = JWT::encode($payload, $key);

        $data_response = [
            "token" => $tokenJWT,
			"token_refresh"=>date("Y-m-d H:i:s",strtotime("+3500 seconds")),
			"id"=>(int)$userInfo['id'],
			"name"=>$userInfo['name'],
			"user_name"=>$userInfo['user_name'],
			"user_type"=>$userInfo['user_type'],
			"email"=>$userInfo['email'],
			"phone_number"=>$userInfo['phone_number'],
			"about"=>$userInfo['about'],
			"profile_pic"=>$userInfo['profile_pic'],
			"created_at"=>date("Y-m-d H:i:s"),
           // "user_info" => $userInfo,
           // "gAuth" => $google_info,
        ];
		
		
		//follower
		$this->followerList( $userInfo['id'], $data_response);
		//Friends		
		$this->friendList( $userInfo['id'], $data_response);
		//Message		
		$this->messageList( $userInfo['id'], $data_response);
		//Post		
		$this->postList( $userInfo['id'], $data_response);

		$response_success["data"] = $data_response;
        return $this->respondCreated($response_success);
    }

    /**
     * Return an array of resource objects, warning about the Method that use.
     *
     * @return mixed
     */
    public function not_valid()
    {
        $response = [
            "status" => 401,
            "error" => true,
            "messages" => "Request not valid",
            "data" => [
                "method" => $this->request->getMethod(true),
            ],
        ];
        return $this->respondCreated($response);
    }

    /**
     * Return an bolean, related to register if not exist.
     *
     * @return mixed
     */
    private function register_user($params)
    {
        $userModel = new UserCayoModel();
        $this->db->transBegin();
        try {
            $username = "user_" . round(microtime(true));
            $dataUser = [
                "active" => 0, //$this->request->getPost('active'),
                "email" => $params["email"],
                "username" => $username,
                "profilepic" => $username . ".jpg",
                "fullname" => $params["name"],
                "password_hash" => password_hash(
                    $params["password"],
                    PASSWORD_DEFAULT
                ),
            ];
            //$id = $this->users->insert(new User($dataUser));
            $user = $userModel
                ->where("email", $params["email"])
                ->select("password_hash")
                ->first();

            if (!isset($user["password_hash"])) {
                $userModel->insert($dataUser);
                //check dir
                $dir = WRITEPATH . "uploads/pp_users";
                if (!is_dir($dir)) {
                    mkdir($dir);
                    log_add("dir:" . $dir . "|created");
                }

                $url = $params["picturepic"];
                $img = WRITEPATH . "uploads/pp_users/" . $username . ".jpg";
                if (ENVIRONMENT == "development") {
                    log_add("url:" . $url . "|to:" . $img);
                }

                file_put_contents($img, file_get_contents($url));
            }

            $this->db->transCommit();
            return 0;
        } catch (\Exception $ex) {
            $this->db->transRollback();
            if (ENVIRONMENT == "development") {
                log_add("error register:" . print_r($ex->getMessage(), 1));
            }
            return 0;
        }
        return 0;
    }

	 /**
     * Return an bolean, related to register if not exist.
     *
     * @return mixed
     */
    private function register_device($data)
	{
		$authModel = new AuthDeviceModel();
        $this->db->transBegin();
        try {
			$this->db->table($authModel->table)
			->replace($data);
			$this->db->transCommit();
            return 0;
        } catch (\Exception $ex) {
            $this->db->transRollback();
            if (ENVIRONMENT == "development") {
                log_add("error register:" . print_r($ex->getMessage(), 1));
            }
            return 0;
        }
        return 0;
	}

    /**
     * Return an array of resource objects, related to user detail on table.
     *
     * @return mixed
     */
    private function detail_user($search = "", $field_search = "id")
    {
        //not started yet
        $tblUsers = new UserCayoModel();
		//"username, email, fullname, active, profilepic, hp, about, dev.user_id user_id, "
		$fields=[
			'usr'=>[
				'username'=>'user_name', 
				'email'=>'email', 
				'id'=>'id',
				//'active', 
		//============NEW===============		
				'fullname'=>'name', 
				'profilepic'=>'profile_pic',
				'hp'=>'phone_number', 
				'about'=>'about',
			],
			'dev'=>[ 
				'platform_id'=>'platform_id',
				'device_ssid'=>'device_ssid',
				'device_model'=>'device_model',
				'device_name'=>'device_system_name',
				'device_system_version'=>'device_system_version',
				'device_name'=>'device_name'
			]
		];
		$aField=[];
		foreach($fields as $alias=>$fieldsTable)
		{
			foreach($fieldsTable as $field=>$alias_name)
			{
				$aField[]="`{$alias}`.`{$field}` as `{$alias_name}`";
			}
		}
		$select_fields = implode(", ", $aField);
		
        $que = $this->db->table($tblUsers->table.' as usr')
            ->where('`usr`.`'.$field_search.'`', $search)
            ->select($select_fields)
			->join("auth_device as dev", "dev.user_id = usr.id", "left")->get();
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			return false;
			die('error:'.print_r($error,1) );
		}
		
		$users= $que->getRowArray();
        
        $configUser = config("User");
        $users["profile_pic_raw"] = $users["profile_pic"];
        $users["profile_pic"] = $configUser->urlProfile . $users["profile_pic"];
		$users['user_type'] = 'user';
        return $users;
    }

	 /**
     * Return an array of resource objects, related to posting on profile.
     *
     * @return mixed
     */
	private function postList($user_id, &$data_response)
	{
		$clUser = new \App\Models\Users\UserPostMdl();
		$queTotal =
			$this->db->table( $clUser->table)
			->where('userId', $user_id )
			//->where('type', 1 ) 
			->select('count(*) c')
			->get();
		$queList =
			$this->db->table( "`{$clUser->table} as src")
			->where('userId', $user_id )
			//->where('type', 1 ) //follow
			->join($this->tblUsers->table." as usr", "src.userId = usr.id", "left")
			->select('usr.username, usr.fullname, src.title, src.messages, src.created_at created')
			->orderBy('src.created_at','desc')
			->limit(10)
			->get();
		
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			$data_response['posts']=[$error];
		}
		else
		{
			$result= $queTotal->getRowArray();
			$data_response['posts'] = [
				'total' =>  (int)@$result['c'],
				'lists' => $queList->getResultArray()
			];
		}
		
		return ;
	}
    
	 /**
     * Return an array of resource objects, related to friend & type on system.
     *
     * @return mixed
     */
	private function friendList($user_id, &$data_response)
	{
		$clType = new \App\Models\Users\TypeFriendMdl();
		$clUser = new \App\Models\Users\UserFriendMdl();
		$queTotal =
			$this->db->table( $clUser->table)
			->where('sourceId', $user_id )
			//->where('type', 1 ) 
			->select('count(*) c')
			->get();
		$queList =
			$this->db->table( "`{$clUser->table} as src")
			->where('sourceId', $user_id )
			//->where('type', 1 ) //follow
			->join($this->tblUsers->table." as usr", "src.targetId = usr.id", "left")
			->join($clType->table." as type", "src.type = type.id", "left")
			->select('usr.username, usr.fullname,type.name as type_name')
			->orderBy('src.created_at','desc')
			->limit(5)
			->get();
		
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			$data_response['friends']=[$error];
		}
		else
		{
			$result= $queTotal->getRowArray();
			$data_response['friends'] = [
				'total' =>  (int)@$result['c'],
				'lists' => $queList->getResultArray()
			];
		}
		
		return ;
	}
    
	/**
     * Return an array of resource objects, related to message to user on system.
     *
     * @return mixed
     */
	private function messageList($user_id, &$data_response)
	{
		$clUser = new \App\Models\Users\UserMessageMdl();
		$queTotal =
			$this->db->table( $clUser->table)
			->where('targetId', $user_id )
			->where('deleted_at', NULL ) 
			->select('count(*) c')
			->get();
		$queList =
			$this->db->table( "`{$clUser->table} as src")
			->where('sourceId', $user_id )
			//->where('type', 1 ) //follow
			->join($this->tblUsers->table." as usr", "src.sourceId = usr.id", "left")
			->where('src.deleted_at', NULL )	
			->where('reads', 0 )			
			->select('usr.username, usr.fullname,src.message as message')
			->orderBy('src.created_at','desc')
			->limit(20) //dibatasi
			->get();
		
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			$data_response['messages']=[$error];
		}
		else
		{
			$result= $queTotal->getRowArray();
			$data_response['messages'] = [
				'total' =>  (int)@$result['c'],
				'unread' => $queList->getResultArray()
			];
		}
		
		return ;
	}
    /**
     * Return an array of resource objects, related to follower on system.
     *
     * @return mixed
     */
	private function followerList($user_id, &$data_response)
	{
		$clUser = new \App\Models\Users\UserFollowerMdl();
		$queTotal =
			$this->db->table( $clUser->table)
			->where('targetId', $user_id )
			->where('type', 1 ) //follow
			->select('count(*) c')
			->get();
		$queList =
			$this->db->table( "`{$clUser->table} as src")
			->where('targetId', $user_id )
			->where('type', 1 ) //follow
			->join($this->tblUsers->table." as usr", "src.sourceId = usr.id", "left")
			->select('usr.username, usr.fullname')
			->orderBy('src.created_at','desc')
			->limit(5)
			->get();
		
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			$data_response['followers']=[$error];
		}
		else
		{
			$result= $queTotal->getRowArray();
			$data_response['followers'] = [
				'total' =>  (int)@$result['c'],
				'lists' => $queList->getResultArray()
			];
		}
		
		return ;
	}
    /**
     * Return info about the page.
     *
     * @return mixed
     */
    public function index()
    {
        if (ENVIRONMENT != "development") {
            throw new \CodeIgniter\Exceptions\PageNotFoundException(
                "Nothing but Blank page here"
            );
        }

        $data = [
            "title" => "Dashboard",
        ];

        return view("auth/info", $data);
    }
}

