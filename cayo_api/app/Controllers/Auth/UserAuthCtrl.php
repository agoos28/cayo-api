<?php
namespace App\Controllers\Auth;

use App\Models\UserCayoModel;
use App\Models\Auths\AuthDeviceModel;

use CodeIgniter\RESTful\ResourceController;
use Exception;
use Firebase\JWT\JWT;

/**
 * Class UserAuthCtrl
 * Route : development/googleAuth, production/googleAuth
 */

class UserAuthCtrl extends ResourceController
{
	protected $users, $tblUsers;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
		$this->tblUsers = new UserCayoModel();
    }


    /**
     * Return an array of resource objects,   related update  profile pic (ONLY)
     *
     * @return mixed
     */
	 public function profilepic_update()
	 {
		$start = microtime(true);
        $request = \Config\Services::request();
        $response_default = [
            "status" => 401,
            "error" => true,
            "messages" => "Access denied",
            "data" => [
                "method" => $this->request->getMethod(true),
            ],
        ];

        $response_success = [
            "status" => 200,
            "error" => false,
            "messages" => "Success",
            "data" => [
                "run" => microtime(true) - $start,
            ],
        ];

        if ($this->request->getMethod(true) != "PUT") {
            $response_default["messages"] = "Method Not Valid";
            return $this->respondCreated($response_default);
        }

        $authHeader = $this->request->getHeader("token");
        $tokenJWT = $authHeader->getValue();

        if ($tokenJWT) {
            try {
                $config = config("User");
                $key = $config->myKey;
                $decoded = JWT::decode($tokenJWT, $key, ["HS256"]);
                if (true) {
                    unset($decoded->token_google);
                    $response = [
                        "status" => 200,
                        "error" => false,
                        "messages" => "Success",
                        "data" => [
                            "run" => microtime(true) - $start,
                            //'detail'=> $decoded
                        ],
                    ];
                    if (isset($decoded->gAuth->email)) {
                        $email = @$decoded->gAuth->email;
                    } elseif (isset($decoded->user_info->email)) {
                        $email = @$decoded->user_info->email;
                    } else {
                        $response = $response_default;
                        $response["messages"] = "Unknown Token";
                        return $this->respondCreated($response);
                    }

                    $user_detail = $this->detail_user($email, "email");
                    if (!isset($user_detail["email"]))
					{
                        $response = $response_default;
                        $response["messages"] = "Unknown User";
                        return $this->respondCreated($response);
                    }

                    //update allow
					$profile_pic = @$decoded->user_info->profile_pic_raw ;
                    $update_field = ["profile_pic"];
                    $fields = [];
                    foreach ($update_field as $field) {
                        $input = $this->request->getVar($field);
                        if ($input != "") {
                            $fields[$field] = $this->request->getVar($field);
                        }
                    }

                    if (count($fields) == 0) {
                        $response = $response_default;
                        $response["messages"] = "No Update";
                        $debug = $this->request->getVar("debug");
                        $response["data"] = [$update_field];
                        return $this->respondCreated($response);
                    }
					else
					{
                        //$profile_pic = $user_detail['profile_pic_raw'];
						$upload = $fields['profile_pic'];
						$img = WRITEPATH . "uploads/pp_users/" . $profile_pic;
						if (ENVIRONMENT == "development") {
							log_add("input:" . strlen($upload) . "|to:" . $img);
						}

						file_put_contents($img, file_get_contents( $upload));
						$response["messages"] = "success update";
                        $response["data"] =
                            "update " . implode(", ", array_keys($fields));
                    }
                    return $this->respondCreated($response);
                } else {
                    $response["messages"] = "Unknown token";
                }
            } catch (Exception $ex) {
                $response = [
                    "status" => 401,
                    "error" => true,
                    "messages" => "Access denied",
                    "data" => [
                        "id" => $id_google,
                        "message" => $ex->getMessage(),
                    ],
                ];
                return $this->respondCreated($response);
            }

            return $this->respondCreated($response);
        }

        $response["messages"] = "Unknown token";
        return $this->respondCreated($response);
	 }

	 /**
     * Return an array of resource objects, related update user info. Not include profile pic
     *
     * @return mixed
     */
    public function user_update()
    {
        $start = microtime(true);
        $request = \Config\Services::request();
        $response_default = [
            "status" => 401,
            "error" => true,
            "messages" => "Access denied",
            "data" => [
                "method" => $this->request->getMethod(true),
            ],
        ];

        $response_success = [
            "status" => 200,
            "error" => false,
            "messages" => "Success",
            "data" => [
                "run" => microtime(true) - $start,
            ],
        ];

        if ($this->request->getMethod(true) != "POST") {
            $response_default["messages"] = "Method Not Valid";
            return $this->respondCreated($response_default);
        }

        $authHeader = $this->request->getHeader("token");
        $tokenJWT = $authHeader->getValue();

        if ($tokenJWT) {
            try {
                $config = config("User");
                $key = $config->myKey;
                $decoded = JWT::decode($tokenJWT, $key, ["HS256"]);
                if (true) {
                    unset($decoded->token_google);
                    $response = [
                        "status" => 200,
                        "error" => false,
                        "messages" => "Success",
                        "data" => [
                            "run" => microtime(true) - $start,
                            //'detail'=> $decoded
                        ],
                    ];
                    if (isset($decoded->gAuth->email)) {
                        $email = @$decoded->gAuth->email;
                    } elseif (isset($decoded->user_info->email)) {
                        $email = @$decoded->user_info->email;
                    } else {
                        $response = $response_default;
                        $response["messages"] = "Unknown Token";
                        return $this->respondCreated($response);
                    }

                    $user_detail = $this->detail_user($email, "email");
                    if (!isset($user_detail["email"])) {
                        $response = $response_default;
                        $response["messages"] = "Unknown User";
                        return $this->respondCreated($response);
                    }

                    //update allow
                    $update_field = ["fullname", "hp", "about"];
                    $fields = [];
                    foreach ($update_field as $field) {
                        $input = $this->request->getVar($field);
                        if ($input != "") {
                            $fields[$field] = $this->request->getVar($field);
                        }
                    }

                    if (count($fields) == 0)
					{
                        $response = $response_default;
                        $response["messages"] = "No Update";
                        $debug = $this->request->getVar("debug");
                        $response["data"] = [$update_field];
                        return $this->respondCreated($response);
                    }
					else
					{
                        $userModel = new UserCayoModel();
                        $userModel
                            ->where("email", $email)
                            ->set($fields)
                            ->update();
                        $response["messages"] = "success update";
                        $response["data"] =
                            "update " . implode(", ", array_keys($fields));
                        $debug = FALSE;//$this->request->getVar("debug");
                        if ($debug) {
                            $response["data"] = $this->detail_user(
                                $email,
                                "email"
                            );
                        }
                    }
                    return $this->respondCreated($response);
                } else {
                    $response["messages"] = "Unknown token";
                }
            } catch (Exception $ex) {
                $response = [
                    "status" => 401,
                    "error" => true,
                    "messages" => "Access denied",
                    "data" => [
                        "id" => $id_google,
                        "message" => $ex->getMessage(),
                    ],
                ];
                return $this->respondCreated($response);
            }

            return $this->respondCreated($response);
        }

        $response["messages"] = "Unknown token";
        return $this->respondCreated($response);
    }

    /**
     * Return an array of resource objects, related to google Auth.
     *
     * @return mixed
     */
    public function user_get($id_google = 1)
    {
        $start = microtime(true);
        $request = \Config\Services::request();
        $response_default = [
            "status" => 401,
            "error" => true,
            "messages" => "Access denied",
            "data" => [
                "method" => $this->request->getMethod(true),
                "id" => $id_google,
            ],
        ];

        $response_success = [
            "status" => 200,
            "error" => false,
            "messages" => "Success",
            "data" => [
                "run" => microtime(true) - $start,
            ],
        ];

        if ($id_google == null) {
            $response_default["messages"] = "Id Not Valid";
            return $this->respondCreated($response_default);
        }

        $tokenJWT = $request->getVar("token");
        if ($tokenJWT) {
            try {
                $config = config("User");
                $key = $config->myKey;
                $decoded = JWT::decode($tokenJWT, $key, ["HS256"]);
                if (true) {
                    unset($decoded->token_google);
                    $response = [
                        "status" => 200,
                        "error" => false,
                        "messages" => "Success",
                        "data" => [
                            "run" => microtime(true) - $start,
                            //'detail'=> $decoded
                        ],
                    ];
                    if (isset($decoded->gAuth->email)) {
                        $email = @$decoded->gAuth->email;
                    } elseif (isset($decoded->user_info->email)) {
                        $email = @$decoded->user_info->email;
                    } else {
                        $response = $response_default;
                        $response["messages"] = "Unknown Token";
                        return $this->respondCreated($response);
                    }

                    $response["data"] = @$decoded->user_info;
					//$this->detail_user($email, "email");
					unset($response["data"]->profile_pic_raw);
					unset($response["data"]->user_type);
                    //$response['data'][]=$decoded;
                } else {
                    $response["messages"] = "Unknown token";
                }
            } catch (Exception $ex) {
                $response = [
                    "status" => 401,
                    "error" => true,
                    "messages" => "Access denied",
                    "data" => [
                        "id" => $id_google,
                        "message" => $ex->getMessage(),
                    ],
                ];
                return $this->respondCreated($response);
            }

            return $this->respondCreated($response);
        }

        $response["messages"] = "Unknown token";
        return $this->respondCreated($response);
    }


    /**
     * Return an array of resource objects, related to user detail on table.
     * Seharusnya tidak digunakan kembali
     * @return mixed
     */
    private function detail_user($search = "", $field_search = "id")
    {
        //not started yet
        $tblUsers = new UserCayoModel();
		//"username, email, fullname, active, profilepic, hp, about, dev.user_id user_id, "
		$fields=[
			'usr'=>[
				'username'=>'user_name', 
				'email'=>'email', 
				'id'=>'id',
				//'active', 
		//============NEW===============		
				'fullname'=>'name', 
				'profilepic'=>'profile_pic',
				'hp'=>'phone_number', 
				'about'=>'about',
			],
			'dev'=>[ 
				'platform_id'=>'platform_id',
				'device_ssid'=>'device_ssid',
				'device_model'=>'device_model',
				'device_name'=>'device_system_name',
				'device_system_version'=>'device_system_version',
				'device_name'=>'device_name'
			]
		];
		$aField=[];
		foreach($fields as $alias=>$fieldsTable)
		{
			foreach($fieldsTable as $field=>$alias_name)
			{
				$aField[]="`{$alias}`.`{$field}` as `{$alias_name}`";
			}
			
		}
		
		$select_fields = implode(", ", $aField);
        $que = $this->db->table($tblUsers->table.' as usr')
            ->where('`usr`.`'.$field_search.'`', $search)
            ->select($select_fields)
			->join("auth_device as dev", "dev.user_id = usr.id", "left")->get();
		$error = $this->db->error() ;
		if( $error['code'] !=0)
		{
			return false;
		}
		
		$users= $que->getRowArray();
        
        $configUser = config("User");
        $users["profile_pic_raw"] = $users["profile_pic"];
        $users["profile_pic"] = $configUser->urlProfile . $users["profile_pic"];
		$users['user_type'] = 'user';
        return $users;
    }

}


