<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	private $table;
	public function __construct()
	{
		$this->table='cy_users';
	}
	
	public function up()
	{
		$db = \Config\Database::connect();
		$forge = \Config\Database::forge();
		$forge->addField([
            'id'          => [
                'type'           => 'bigint',
                'constraint' 	 => '20',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '120',
            ],
            'email'       => [
                'type'       => 'CHAR',
                'constraint' => '100',
            ],
            'email'=>array( 'type' => 'VARCHAR',  'constraint' => '100','unique'=> TRUE),
			'phone_no'=>array( 'type' => 'VARCHAR',  'constraint' => '30'),
			'password'=>array( 'type' => 'VARCHAR',  'constraint' => '150'),
			'created_at'=>array( 'type' => 'timestamp'),
			'modifield_at'=>array( 'type' => 'INT')
        ]);
        //$forge->addKey('blog_id', true);
        //$forge->createTable('blog');
		$forge->addKey('id', TRUE);
		$forge->addKey('name', FALSE);
		$forge->createTable($this->table, TRUE);
		$str = $db->getLastQuery();		 
		log_message("info", "create table:$str");
		$sql = "ALTER TABLE `{$this->table}` 
		CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		CHANGE `modifield_at` `modifield_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL;";
		$db->query($sql);
		$str = $db->getLastQuery();		 
		log_message("info", "update table {$this->table} field:$str");
		$sql="ALTER TABLE `{$this->table}` DEFAULT CHARSET=latin1 COLLATE latin1_swedish_ci;";
		$db->query($sql);
		$str = $db->getLastQuery();		 
		log_message("info", "update table {$this->table} field:$str");
		//$db->resetQuery();
	}

	public function down()
	{
		//
	}
}
