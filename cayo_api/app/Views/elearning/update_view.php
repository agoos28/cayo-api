<!-- Include -->
<?= $this->include('agungsugiarto\boilerplate\Views\load\select2') ?>
<!-- Extend from layout index -->
<?= $this->extend('agungsugiarto\boilerplate\Views\layout\index') ?>

<!-- Section content -->
<?= $this->section('content') ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <div class="float-left">
                    <div class="btn-group">
                        <a href="<?= route_to('student') ?>" class="btn btn-sm btn-block btn-secondary"><i class="fas fa-arrow-left"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="<?= base_url('student/edit/'.$student['id']) ?>/update" method="post" class="form-horizontal">
                    <?= csrf_field() ?>
                    <input type="hidden" name="_method" value="put" />

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label"><?=lang('Auth.email')?></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control <?= session('error.email') ? 'is-invalid' : '' ?>" value="<?= $student['email'] ?>" placeholder="<?=lang('Auth.email')?>" autocomplete="off">
                                <?php if (session('error.email')) { ?>
                                <div class="invalid-feedback">
                                    <h6><?= session('error.email') ?></h6>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-student"></i></span>
                                </div>
                                <input type="text" name="name" class="form-control <?= session('error.name') ? 'is-invalid' : '' ?>" value="<?= $student['name'] ?>" placeholder="<?=lang('Auth.studentname')?>" autocomplete="off">
                                <?php if (session('error.name')) { ?>
                                <div class="invalid-feedback">
                                    <h6><?= session('error.name') ?></h6>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Telepon</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-student"></i></span>
                                </div>
                                <input type="text" name="mobile" class="form-control <?= session('error.studentphone') ? 'is-invalid' : '' ?>" value="<?= $student['mobile'] ?>" placeholder="<?=lang('Auth.mobile')?>" autocomplete="off">
                                <?php if (session('error.mobile')) { ?>
                                <div class="invalid-feedback">
                                    <h6><?= session('error.mobile') ?></h6>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Branch</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-student"></i></span>
                                </div>
                                <input type="text" name="branch" class="form-control <?= session('error.studentphone') ? 'is-invalid' : '' ?>" value="<?= $student['branch'] ?>" placeholder="<?=lang('Auth.studentname')?>" autocomplete="off">
                                <?php if (session('error.branch')) { ?>
                                <div class="invalid-feedback">
                                    <h6><?= session('error.branch') ?></h6>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
 
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <div class="float-right">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-sm btn-block btn-primary">
                                        <?= lang('boilerplate.global.save') ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
  <script>
  $('.select').select2();
  </script>
<?= $this->endSection() ?>

