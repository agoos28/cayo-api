<?= $this->include('agungsugiarto\boilerplate\Views\load\select2') ?>
<?= $this->include('agungsugiarto\boilerplate\Views\load\datatables') ?>
<!-- Extend from layout index -->
<?= $this->extend('agungsugiarto\boilerplate\Views\layout\index') ?>

<!-- Section content -->
<?= $this->section('content') ?>
<!-- SELECT2 EXAMPLE -->
<div class="card card-default">
    <div class="card-header">
        <div class="card-tools">
            <div class="btn-group">
                <a href="<?= base_url('student/download-report') ?>" class="btn btn-info pull-right" style="margin-top: -7px;">Download Excel Report</a>				
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">


				  <div class="panel panel-primary">
					<div class="panel-heading">
					   Student Report
					   
					</div>
					<div class="panel-body">
					
<?php
                if (session()->get("success")) {
?>
						<div class="alert alert-success">
							<?= session()->get("success") ?>
						</div>
<?php
				}
?>
					<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?= site_url('student/upload-student') ?>" id="frm-add-students">
						<div class="form-group">
							<label class="control-label col-sm-2" for="file">File:</label>
							<div class="col-sm-10">
								<input type="file" class="form-control" required id="file" placeholder="Enter file" name="file">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
					</div>
				  </div>


				  <table class="table table-striped" id="table-student">
					<thead>
					  <tr>
						<th>ID</th>
						<th>Name</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Branch</th>
						<th>Di buat</th>
						<th>Option</th>
					  </tr>
					</thead>
					<tbody>

					  <?php
						if(count($students) > 0){

						  $count = 1;

						  foreach($students as $student){
							?>
							  <tr>
								<td><?= $count++ ?></td>
								<td><?= $student['name'] ?></td>
								<td><?= $student['email'] ?></td>
								<td><?= $student['mobile'] ?></td>
								<td><?= $student['branch'] ?></td>
							  </tr>
							<?php
						  }
						}
					  ?>
					</tbody>
				  </table>
				
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.card -->
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
$(function() {

    var tableUser = $('#table-student').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        order: [[1, 'asc']],
		"lengthMenu": [[10, 15, 25, 50, 5], [10, 15, 25, 50, "5"]],
        ajax: {
            url: '<?= base_url(route_to("student")) ?>',
            method: 'POST'
        },
        columnDefs: [{
            orderable: false,
            targets: [0,3,5]
        }],
        columns: [{
                'data': null
            },
            {
                'data': 'name'
            },
            {
                'data': 'email'
            },
            {
                'data': 'mobile'
            },
            {
                'data': 'branch'
            },
            // {
                // 'data': function(data) {
                    // return `<span class="badge ${data.active == 1 ? 'bg-success' : 'bg-danger'}">${data.active == 1 ? '<?= lang('boilerplate.user.fields.active') ?>' : '<?= lang('boilerplate.user.fields.non_active') ?>'}</span>`
                // }
            // },
            {
                'data': 'created_at',
                'render': function(data) {
                    return moment(data).fromNow()
                }
            },
            {
                "data": function(data) {
                    return `<td class="text-right py-0 align-middle">
                            <div class="btn-group btn-group-sm">
                                <a href="<?= route_to('student') ?>/edit/${data.id}" class="btn btn-primary btn-edit"><i class="fas fa-pencil-alt"></i></a>
                                <button class="btn btn-danger btn-delete" data-id="${data.id}"><i class="fas fa-trash"></i></button>
                            </div>
                            </td>`
                }
            }
        ]
    });

    tableUser.on('draw.dt', function() {
        var PageInfo = $('#table-student').DataTable().page.info();
        tableUser.column(0, {
            page: 'current'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

    $(document).on('click', '.btn-delete', function(e) {
        Swal.fire({
                title: '<?= lang('boilerplate.global.sweet.title') ?>',
                text: "<?= lang('boilerplate.global.sweet.text') ?>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<?= lang('boilerplate.global.sweet.confirm_delete') ?>'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        url: `<?= base_url('student/delete') ?>/${$(this).attr('data-id')}`,
                        method: 'DELETE',
                    }).done((data, textStatus, jqXHR) => {
                        Toast.fire({
                            icon: 'success',
                            title: jqXHR.statusText,
                        });
                        tableUser.ajax.reload();
                    }).fail((error) => {
                        Toast.fire({
                            icon: 'error',
                            title: error.responseJSON.messages.error,
                        });
                    })
                }
            })
    });

    tableUser.on('order.dt search.dt', () => {
        tableUser.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
});
</script>
<?= $this->endSection() ?>