<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

/**
 * -------------------------------------------------------------------
 * User
 * -------------------------------------------------------------------
 *
 */
class User extends BaseConfig
{
	/**
	 * -------------------------------------------------------------------
	 * OS Platforms
	 * -------------------------------------------------------------------
	 *
	 * @var array<string, string>
	 */
	public $myKey = 'mysecretkey';
	/*
	* Untuk keperluan debug saja
	*/
	public $access_token=NULL;
	public $expires_in=NULL;
	public $scope=NULL;
	public $token_type=NULL;
	public $d_token=NULL; 
	public $created=NULL;
	public $defPassword="Joker";//di local saja
	public $urlProfile=NULL;
}