<?php
/***
Logs:

2022/01/08 Membuat dasar Routes production
2022/01/18 Menambah GoogleAuth

****/

$othersRoutes=[ 'price', 'banner', 'GoogleAuth' ]; //gunakan nama sesuai parentnya

//format penamaan controller dan register dapat melihat versi development


foreach( $othersRoutes as $route_file )
{
	if(is_file( __DIR__ . DIRECTORY_SEPARATOR . ucfirst($route_file) . '.php'))
	{
		include_once( __DIR__ . DIRECTORY_SEPARATOR . ucfirst($route_file) . '.php' ) ;
		$info=[ 'route'=>$route_file ];
		log_message('info','Production| load routes {route}', $info);
	}

	if( is_file( __DIR__ . DIRECTORY_SEPARATOR . trim($route_file) . '.php') )
	{
		include_once( __DIR__ . DIRECTORY_SEPARATOR . trim($route_file) . '.php' ) ;
		$info=[ 'route'=>$route_file ];
		log_message('info','Production| load routes {route}', $info);

	}
	
}