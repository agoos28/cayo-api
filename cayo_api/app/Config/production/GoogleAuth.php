<?php

$routes->group("authentication", function ($routes) { 
	$routes->get('/', 'Auth\GoogleAuthCtrl::index');
    $routes->get( "user/(:num)",  "Auth\GoogleAuthCtrl::user_get");
    $routes->post( "user/(:num)",  "Auth\GoogleAuthCtrl::user_get");
    $routes->post( "update-user",     "Auth\GoogleAuthCtrl::user_update"); //harusnya post
    $routes->post( "signin-with-google",     "Auth\GoogleAuthCtrl::signin_post");
    $routes->get( "signin-with-google",     "Auth\GoogleAuthCtrl::not_valid");
});

$routes->get( "uploads/user",  "Auth\UploadsCtrl::fileUsers");