<?php 
$routes->group("gAuth", function ($routes) { 
	$routes->get('/', 'Auth\GoogleAuthCtrl::index');
    $routes->get( "user/(:alpha)",  "Auth\GoogleAuthCtrl::user_get");
    $routes->patch( "update",     "Auth\GoogleAuthCtrl::user_update");
    $routes->post( "signinWithGoogle",     "Auth\GoogleAuthCtrl::signin_post");
    $routes->get( "signinWithGoogle",     "Auth\GoogleAuthCtrl::not_valid");
});
//authentication/signin-with-google (untuk production)
$routes->group("authentication", function ($routes) { 
	$routes->get('/', 'Auth\GoogleAuthCtrl::index');
    $routes->post("signin-with-google",	"Auth\GoogleAuthCtrl::signin_post");
    $routes->get( "signin-with-google",	"Auth\GoogleAuthCtrl::not_valid");
	
	$routes->get( "user-detail",	"Auth\GoogleAuthCtrl::not_valid");
	$routes->get( "user-detail/(:num)", "Auth\UserAuthCtrl::user_get/$1");
    $routes->post( "update-user",	"Auth\UserAuthCtrl::user_update"); //selain gambar
    $routes->put( "update-user",	"Auth\UserAuthCtrl::profilepic_update"); //harusnya post

});
$routes->get( "uploads/user",  "Auth\UploadsCtrl::fileUsers");
$routes->match(['get', 'post'], 'imageRender/user/(:segment)', 'Auth\UploadsCtrl::fileUsers/$1');