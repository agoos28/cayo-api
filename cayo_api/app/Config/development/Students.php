<?php
 
$routes->group("student", function ($routes) 
{
	//halaman utama
    $routes->get("/", "ELearn\StudentController::index");
	//json datatable
    $routes->post("/", "ELearn\StudentController::index");
	//edit dan update
	$routes->get("edit/(:num)", "ELearn\StudentController::editPage/$1");
	$routes->put("edit/(:num)/update", "ELearn\StudentController::updatePage/$1");
	//delete data	
	$routes->delete("delete/(:num)", "ELearn\StudentController::deletePage/$1");
	
	//export (download) dan import data memakai excel
    $routes->get("download-report", "ELearn\StudentController::downloadExcelReport");
	$routes->match(["get", "post"], "upload-student", "ELearn\StudentController::uploadStudent");
	

});
