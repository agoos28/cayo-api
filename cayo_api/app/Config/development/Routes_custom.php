<?php 
/***
Logs:

2022/01/08 Membuat dasar Routes Development
2022/01/18 Menambah GoogleAuth

****/
//disarankan memakai nama UserCtrl dibandingkan hanya User
//format penamaan controller
$routes->group("contoh", function ($routes) {
    $routes->post("register", "ContohCtrl::register");
	$routes->get("profile",   "ContohCtrl::details");
});
//gunakan nama sesuai parentnya. Nama File memakai format ucfirst 
$othersRoutes=['harley', 'joker','userRoutes', 'googleAuth', 'loginRoute', 'Students' ];  
//
foreach( $othersRoutes as $route_file )
{
	if( is_file( __DIR__ . DIRECTORY_SEPARATOR . ucfirst($route_file) . '.php') )
	{
		include_once( __DIR__ . DIRECTORY_SEPARATOR . ucfirst($route_file) . '.php' ) ;
		$info=[ 'route'=>$route_file ];
		log_message('info','Devel| load routes {route}', $info);

	}
	/*
	if( is_file( __DIR__ . DIRECTORY_SEPARATOR . trim($route_file) . '.php') )
	{
		include_once( __DIR__ . DIRECTORY_SEPARATOR . trim($route_file) . '.php' ) ;
		$info=[ 'route'=>$route_file ];
		log_message('info','Devel| load routes {route}', $info);

	}
	*/
}
