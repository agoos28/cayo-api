<?php 
$routes->group("loginExt", function ($routes) { 
	$routes->get('/', 'Auth\UserCtrl::index');
    $routes->get( "profile",  "Auth\UserCtrl::details");
    $routes->get( "detail",     "Auth\UserCtrl::details");
    $routes->get( "info",     "Auth\UserCtrl::info");
});