<?php 
$routes->group("api", function ($routes) {
    $routes->post("register", "Api\UserCtrl::register");
    $routes->post("login",    "Api\UserCtrl::login");
    $routes->get( "profile",  "Api\UserCtrl::details");
    $routes->get( "info",     "Api\UserCtrl::info");
});