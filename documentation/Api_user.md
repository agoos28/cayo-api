API user ini untuk development. Apabila untuk versi production, 
silahkan membaca pada folder <b>live</b>. 
Api berikut ini tidak akan hadir dalam versi live. Isinya akan disesuaikan dan berubah

<h4>Register Demo</h4>
url: <b>api/register</b>

params: 
 
* name : wajib ada
* email : dalam bentuk email
* phone_no : wajib ada dan disarankan bentuk telepon normal
* password : wajib ada. minimal 6 karakter . Pada versi live, input password akan di periksa dengan detail

return 

* status: <b>200</b>
* error: false
* data: object
* message: <b>pesan</b>

<h4>Login Demo</h4>
url: <b>api/login</b>

params: 
 
* email : dalam bentuk email
* password : wajib ada. minimal 6 karakter

return 

* status: <b>200</b>
* error: false
* data: object token. Tidak menampilkan info lain.
* message: <b>pesan</b>


<h4>Detail Demo</h4>
url: <b>api/profile</b>

headers: 
 
* Content-type : application/json
* Accept : application/json
* Token : dari token di atas

return 

* status: <b>200</b>
* error: false
* data: object profile. Input yang hadir akan dibatasi
* message: <b>pesan</b>
