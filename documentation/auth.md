API Auth ini untuk development dan production, 
silahkan membaca pada folder <b>live</b>. 
Api berikut ini tidak akan hadir dalam versi live. Isinya akan disesuaikan dan berubah

<h4>SignInWithGoogle</h4>
url: <b>authentication/signin-with-google</b>

sumber: table auth_device

Input:
<pre>
export interface SignInWithGoogle {
  token: string; //tokennya si google.
  platformId: PlatformId;
  deviceSsid: string;
  deviceModel?: string;
  deviceSystemName?: string;
  deviceSystemVersion?: string;
  deviceName?: string;
}
</pre>

Output /kembalian login
<pre>
data: {
  token: string;
  token_refresh: string;
  id: number;
  name: string;
  user_name: string;
  user_type: string;
  email: string;
  phone_number: string;
  about: string;
  created_at: string;
}
</pre>