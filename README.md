<h2>CAYO API</h2>
adalah API yang dijalankan menggunakan CI 4

<h3>Cara menggunakan</h3>
<ol>
<li>masuk ke alamat {site}loginExt/detail</li>
<li>login ke gmail</li>
<li>dapatkan token</li>
<li>gunakan token pada {site}loginExt/info dengan parameter token</li>
</ol>

<h3>Cara pemasangan</h3>
<ol>
<li>copy _index.php menjadi index.php</li>
<li>masuk cayo_api.</li>
<li>jalankan composer update</li>
<li>copy env menjadi .env</li>
<li>lakukan edit sesuai kebutuhan</li>
<li>Masuk folder app</li>
<li>edit config didalamnya</li>
<li>Buat databasenya</li>
<li>Jalankan <pre>php spark migrate</pre></li>
<li>Apabila di hosting. Hapus folder dokumentasi apabila tersedia</li>
</ol>

<h3>Proses Input-Output API</h3>

Dalam pengembangan

Detail API lainnya silahkan merujuk pada folder <b>Dokumentasi</b>
